###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 03b - Animal Farm 1
#
# @file Makefile
# @version 1.0
#
# @author Vinton Sistoza <wiliki@hawaii.edu>
# @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
# @date   1/28/21
##############################################################################

animalfarm: cat.o animals.o main.o
	cc cat.o animals.o main.o -o animalfarm

main.o: cat.h

cat.o: cat.h animals.h

animals.o: animals.h

clean:
	rm -f *.o animalfarm

///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>

#include "animals.h"

/// Decode the enum Color into strings for printf()
char* colorName (int i) 
{
   switch(i)
   {
      case 0: return "BLACK";
         break;
      case 1: return "WHITE";
         break;
      case 2: return "RED";
         break;
      case 3: return "BLUE";
         break;
      case 4: return "GREEN";
         break;
      case 5: return "PINK";
         break;
      default: return NULL;
         break;
   }
}

char* genderClass(int i)
{
   switch(i)
      {
         case 0: return "MALE";
            break;
         case 1: return "FEMALE";
            break;
         default: return NULL;
            break;
      }

}

char* catSpecies(int i)
{
   switch(i)
   {
      case 0: return "MAIN_COON";
         break;
      case 1: return "MANX";
         break;
      case 2: return "SHORTHAIR";
         break;
      case 3: return "PERSIAN";
         break;
      case 4: return "SPHYX";
         break;
      default: return NULL;
         break;
   }
}
char* isFixed(int i)
{
   switch(i)
   {
      case 0: return "NO";
         break;
      case 1: return "YES";
         break;
      defualt: return NULL;
         break;
   }

}


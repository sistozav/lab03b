///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file cat.c
/// @version 1.0
///
/// Implements a simple database that manages cats
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <string.h>
#include <stdbool.h>
#include <stdio.h>

#include "animals.h"
#include "cat.h"

// @todo declare an array of struct Cat, call it catDB and it should have at least MAX_SPECIES records in it
enum Colors {BLACK,WHITE,RED,BLUE,GREEN,PINK};
enum CatBreeds {MAIN_COON,MANX,SHORTHAIR,PERSIAN,SPHYNX};
enum Gender {MALE,FEMALE};

struct catz
{
   char name[30];
   enum CatBreeds breed;
   enum Gender gender;
   enum Colors collar1_color;
   enum Colors collar2_color;
   float weight;
   bool isFixed;
   long license;
};
/// Add Alice to the Cat catabase at position i.
/// 
/// @param int i Where in the catDB array that Alice the cat will go
///
/// @note This terrible style... we'd never hardcode this data, but it gets us started.

struct catz AllCatz[MAX_SPECIES];

void addAliceTheCat(int i) 
{

   strcpy(AllCatz[0].name, "alice");
   AllCatz[0].breed = 1;
   AllCatz[0].isFixed = true;
   AllCatz[0].gender = 0;
   AllCatz[0].weight = 12.34;
   AllCatz[0].collar1_color = 0;
   AllCatz[0].collar2_color = 2;
   AllCatz[0].license = 12345;
}



/// Print a cat from the database
/// 
/// @param int i Which cat in the database that should be printed
///

void printCat(int i) 
{
   // Here's a clue of what one printf() might look like...
   // printf ("    collar color 1 = [%s]\n", colorName(catDB[i].collar1_color ));
   printf ("Cat Name = [%s]\n",AllCatz[0].name);
   printf ("    gender = [%s]\n",genderClass(AllCatz[0].breed));
   printf ("    collar color 1 = [%s]\n", colorName(AllCatz[0].collar1_color));
   printf ("    collar color 2 = [%s]\n", colorName(AllCatz[0].collar2_color));
   printf ("    breed = [%s]\n",catSpecies(AllCatz[0].gender));
   printf ("    isFixed = [%s]\n",isFixed(AllCatz[0].isFixed));
   printf ("    weight = [%4.2f]\n",AllCatz[0].weight);
   printf ("    license = [%d]\n",AllCatz[0].license);
}

